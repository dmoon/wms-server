package com.zhiche.wms.dto.opbaas.paramdto;

import com.zhiche.wms.domain.model.otm.OtmOrderRelease;

public class OrderReleaseParamDTO extends OtmOrderRelease {

    private String shipmentType; //运输指令类型
    private String transportModeGid; //运输方式编码
    private String serviceProviderGid; //分供方编码
    private String serviceProviderName; //分供方名称
    private String driverGid; //司机编码
    private String driverName; //司机姓名
    private String driverPhone; //司机联系方式
    private String plateNo; //车牌号/车厢号/船号
    private String trailerNo; //挂车号
    private String boundType; //出入库类型(10:入库,20:出库,30:移库,40:其他)
    private String taskType; //任务类型
    private String taskStatus; //任务状态
    private String inboundStatus; //入库状态
    private String outboundStatus; //出库状态
    private String searchStatus; //寻车任务状态
    private String moveStatus; //移车任务状态
    private String pickStatus; //提车任务状态

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getTransportModeGid() {
        return transportModeGid;
    }

    public void setTransportModeGid(String transportModeGid) {
        this.transportModeGid = transportModeGid;
    }

    public String getServiceProviderGid() {
        return serviceProviderGid;
    }

    public void setServiceProviderGid(String serviceProviderGid) {
        this.serviceProviderGid = serviceProviderGid;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getDriverGid() {
        return driverGid;
    }

    public void setDriverGid(String driverGid) {
        this.driverGid = driverGid;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getBoundType() {
        return boundType;
    }

    public void setBoundType(String boundType) {
        this.boundType = boundType;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getInboundStatus() {
        return inboundStatus;
    }

    public void setInboundStatus(String inboundStatus) {
        this.inboundStatus = inboundStatus;
    }

    public String getOutboundStatus() {
        return outboundStatus;
    }

    public void setOutboundStatus(String outboundStatus) {
        this.outboundStatus = outboundStatus;
    }

    public String getSearchStatus() {
        return searchStatus;
    }

    public void setSearchStatus(String searchStatus) {
        this.searchStatus = searchStatus;
    }

    public String getMoveStatus() {
        return moveStatus;
    }

    public void setMoveStatus(String moveStatus) {
        this.moveStatus = moveStatus;
    }

    public String getPickStatus() {
        return pickStatus;
    }

    public void setPickStatus(String pickStatus) {
        this.pickStatus = pickStatus;
    }
}
