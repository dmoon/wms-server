package com.zhiche.wms.dto.interfacedto;

import java.io.Serializable;

public class InboundNoticeFromTMSDTO implements Serializable {
    private String orderno;
    private String zorderno;
    private String custOrderno;
    private String custShipno;
    private String waybillCode;
    private String customer;
    private String origin;
    private String dest;
    private String vin;
    private String engine;
    private String style;
    private String styleDesc;
    private String factory_whno;
    private String styleId;//车型id
    private String stocktransfer;  //是否移库单标识
    private String od_status;//用来标识订单的状态(DELETED 删除 MODIFY 修改)


    public String getZorderno() {
        return zorderno;
    }

    public void setZorderno(String zorderno) {
        this.zorderno = zorderno;
    }

    public String getOd_status() {
        return od_status;
    }

    public void setOd_status(String od_status) {
        this.od_status = od_status;
    }

    public String getStocktransfer() {
        return stocktransfer;
    }

    public void setStocktransfer(String stocktransfer) {
        this.stocktransfer = stocktransfer;
    }

    public String getFactory_whno() {
        return factory_whno;
    }

    public void setFactory_whno(String factory_whno) {
        this.factory_whno = factory_whno;
    }

    public String getStyleId() {
        return styleId;
    }

    public void setStyleId(String styleId) {
        this.styleId = styleId;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCustOrderno() {
        return custOrderno;
    }

    public void setCustOrderno(String custOrderno) {
        this.custOrderno = custOrderno;
    }

    public String getCustShipno() {
        return custShipno;
    }

    public void setCustShipno(String custShipno) {
        this.custShipno = custShipno;
    }

    public String getWaybillCode() {
        return waybillCode;
    }

    public void setWaybillCode(String waybillCode) {
        this.waybillCode = waybillCode;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getStyleDesc() {
        return styleDesc;
    }

    public void setStyleDesc(String styleDesc) {
        this.styleDesc = styleDesc;
    }
}
