package com.zhiche.wms.dto.opbaas.resultdto;

import com.zhiche.wms.domain.model.opbaas.ComponentConfig;

import java.io.Serializable;
import java.util.List;

/**
 * 缺件登记详情 dto
 * @author hxh
 * @since 2018-08-07
 */

public class MissingRegidterDetailDTO implements Serializable {

    private String id;
    private String level1Code;
    private String level1Name;
    private String parentCode;
    private String level;
    private String sort;
    private String status;
    private String gmtCreate;
    private String gmtModified;
    private List<ComponentConfig> childComponent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel1Code() {
        return level1Code;
    }

    public void setLevel1Code(String level1Code) {
        this.level1Code = level1Code;
    }

    public String getLevel1Name() {
        return level1Name;
    }

    public void setLevel1Name(String level1Name) {
        this.level1Name = level1Name;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public List<ComponentConfig> getChildComponent() {
        return childComponent;
    }

    public void setChildComponent(List<ComponentConfig> childComponent) {
        this.childComponent = childComponent;
    }
}
