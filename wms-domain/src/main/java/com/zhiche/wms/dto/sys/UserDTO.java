package com.zhiche.wms.dto.sys;

import com.zhiche.wms.domain.model.sys.User;

import java.io.Serializable;

public class UserDTO extends User implements Serializable {

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
