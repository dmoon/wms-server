package com.zhiche.wms.dto.outbound;

import java.io.Serializable;

public class OutboundShipListDTO implements Serializable {

    private String prepareNo;
    private String lotNo1;
    private String prepareStatus;
    private String preparetor;
    private String noticeLineId;
    private String noticeHeadId;
    private String noticeLineStatus;
    private String loadingArea;
    private String locationNo;
    private String shipSpace;
    private String noticeHeadNo;
    private String ownerId;
    private String noticeHeadSourceNo;
    private String materielCode;
    private String materielName;
    private String houseId;
    private String houseName;
    private String ownerOrderNo;
    private String materielId;

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getNoticeHeadSourceNo() {
        return noticeHeadSourceNo;
    }

    public void setNoticeHeadSourceNo(String noticeHeadSourceNo) {
        this.noticeHeadSourceNo = noticeHeadSourceNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getNoticeHeadNo() {
        return noticeHeadNo;
    }

    public void setNoticeHeadNo(String noticeHeadNo) {
        this.noticeHeadNo = noticeHeadNo;
    }


    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getShipSpace() {
        return shipSpace;
    }

    public void setShipSpace(String shipSpace) {
        this.shipSpace = shipSpace;
    }

    public String getLoadingArea() {
        return loadingArea;
    }

    public void setLoadingArea(String loadingArea) {
        this.loadingArea = loadingArea;
    }

    public String getNoticeLineStatus() {
        return noticeLineStatus;
    }

    public void setNoticeLineStatus(String noticeLineStatus) {
        this.noticeLineStatus = noticeLineStatus;
    }

    public String getPrepareNo() {
        return prepareNo;
    }

    public void setPrepareNo(String prepareNo) {
        this.prepareNo = prepareNo;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }


    public String getPrepareStatus() {
        return prepareStatus;
    }

    public void setPrepareStatus(String prepareStatus) {
        this.prepareStatus = prepareStatus;
    }

    public String getPreparetor() {
        return preparetor;
    }

    public void setPreparetor(String preparetor) {
        this.preparetor = preparetor;
    }

    public String getNoticeLineId() {
        return noticeLineId;
    }

    public void setNoticeLineId(String noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public String getNoticeHeadId() {
        return noticeHeadId;
    }

    public void setNoticeHeadId(String noticeHeadId) {
        this.noticeHeadId = noticeHeadId;
    }

}
