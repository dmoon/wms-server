package com.zhiche.wms.domain.model.otm;

import java.io.Serializable;
import java.util.List;

public class ShipmentOTMResultDTO implements Serializable {
    private String allCheck;
    private String allRemark;
    private List<ShipmentOTMDetailDTO> details;

    public String getAllCheck() {
        return allCheck;
    }

    public void setAllCheck(String allCheck) {
        this.allCheck = allCheck;
    }

    public String getAllRemark() {
        return allRemark;
    }

    public void setAllRemark(String allRemark) {
        this.allRemark = allRemark;
    }

    public List<ShipmentOTMDetailDTO> getDetails() {
        return details;
    }

    public void setDetails(List<ShipmentOTMDetailDTO> details) {
        this.details = details;
    }

    private class ShipmentOTMDetailDTO {
        private String detailCheck;
        private String detailRemark;

        public String getDetailCheck() {
            return detailCheck;
        }

        public void setDetailCheck(String detailCheck) {
            this.detailCheck = detailCheck;
        }

        public String getDetailRemark() {
            return detailRemark;
        }

        public void setDetailRemark(String detailRemark) {
            this.detailRemark = detailRemark;
        }
    }
}
