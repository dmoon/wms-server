package com.zhiche.wms.domain.model.log;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 接口导出日志明细
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
@TableName("w_itf_explog_line")
public class ItfExplogLine extends Model<ItfExplogLine> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 日志头ID
     */
    @TableField("header_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long headerId;
    /**
     * 导出类型(10:入库导出,20:出库导出)
     */
    @TableField("export_type")
    private String exportType;
    /**
     * 相关数据ID
     */
    @TableField("relation_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long relationId;
    /**
     * 目标系统(10:ILS)
     */
    @TableField("target_source")
    private String targetSource;
    /**
     * 接口地址
     */
    @TableField("interface_url")
    private String interfaceUrl;
    /**
     * 导出开始时间
     */
    @TableField("export_start_time")
    private Date exportStartTime;
    /**
     * 导出完成时间
     */
    @TableField("export_end_time")
    private Date exportEndTime;
    /**
     * 导出状态(1:成功,0:失败)
     */
    @TableField("export_status")
    private String exportStatus;
    /**
     * 导出备注
     */
    @TableField("export_remarks")
    private String exportRemarks;
    /**
     * 数据内容
     */
    @TableField("data_content")
    private String dataContent;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField("request_id")
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Long headerId) {
        this.headerId = headerId;
    }

    public String getExportType() {
        return exportType;
    }

    public void setExportType(String exportType) {
        this.exportType = exportType;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public String getTargetSource() {
        return targetSource;
    }

    public void setTargetSource(String targetSource) {
        this.targetSource = targetSource;
    }

    public String getInterfaceUrl() {
        return interfaceUrl;
    }

    public void setInterfaceUrl(String interfaceUrl) {
        this.interfaceUrl = interfaceUrl;
    }

    public Date getExportStartTime() {
        return exportStartTime;
    }

    public void setExportStartTime(Date exportStartTime) {
        this.exportStartTime = exportStartTime;
    }

    public Date getExportEndTime() {
        return exportEndTime;
    }

    public void setExportEndTime(Date exportEndTime) {
        this.exportEndTime = exportEndTime;
    }

    public String getExportStatus() {
        return exportStatus;
    }

    public void setExportStatus(String exportStatus) {
        this.exportStatus = exportStatus;
    }

    public String getExportRemarks() {
        return exportRemarks;
    }

    public void setExportRemarks(String exportRemarks) {
        this.exportRemarks = exportRemarks;
    }

    public String getDataContent() {
        return dataContent;
    }

    public void setDataContent(String dataContent) {
        this.dataContent = dataContent;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ItfExplogLine{" +
                ", id=" + id +
                ", headerId=" + headerId +
                ", exportType=" + exportType +
                ", relationId=" + relationId +
                ", targetSource=" + targetSource +
                ", interfaceUrl=" + interfaceUrl +
                ", exportStartTime=" + exportStartTime +
                ", exportEndTime=" + exportEndTime +
                ", exportStatus=" + exportStatus +
                ", exportRemarks=" + exportRemarks +
                ", dataContent=" + dataContent +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
