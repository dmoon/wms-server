package com.zhiche.wms.domain.mapper.outbound;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.outbound.OutboundShipHeader;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.dto.outbound.OutboundShipListDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 出库单头 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface OutboundShipHeaderMapper extends BaseMapper<OutboundShipHeader> {

    List<OutboundShipListDTO> selectPageShipList(Page<OutboundShipListDTO> page,
                                                 @Param("ew") Wrapper<OutboundShipListDTO> wp);
}
