package com.zhiche.wms.domain.model.movement;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 移位单头
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@TableName("w_movement_header")
public class MovementHeader extends Model<MovementHeader> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<MovementLine> movementLineList;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 移位单号
     */
    @TableField("movement_no")
    private String movementNo;
    /**
     * 仓库
     */
    @TableField("store_house_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 单据日期
     */
    @TableField("order_date")
    private Date orderDate;
    /**
     * 业务类型(10::入库移位,20:出库移位,30:调整移位)
     */
    @TableField("business_type")
    private String businessType;
    /**
     * 业务单据ID
     */
    @TableField("business_doc_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long businessDocId;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 状态(10:保存,20:审核,30:撤销)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMovementNo() {
        return movementNo;
    }

    public void setMovementNo(String movementNo) {
        this.movementNo = movementNo;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Long getBusinessDocId() {
        return businessDocId;
    }

    public void setBusinessDocId(Long businessDocId) {
        this.businessDocId = businessDocId;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MovementHeader{" +
                ", id=" + id +
                ", movementNo=" + movementNo +
                ", storeHouseId=" + storeHouseId +
                ", ownerId=" + ownerId +
                ", orderDate=" + orderDate +
                ", businessType=" + businessType +
                ", businessDocId=" + businessDocId +
                ", lineCount=" + lineCount +
                ", status=" + status +
                ", remarks=" + remarks +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }

    public List<MovementLine> getMovementLineList() {
        return movementLineList;
    }

    public void setMovementLineList(List<MovementLine> movementLineList) {
        this.movementLineList = movementLineList;
    }

    public void addMovementLine(MovementLine movementLine) {
        if (Objects.isNull(movementLineList)) {
            movementLineList = new ArrayList<>();
        }
        movementLineList.add(movementLine);
    }
}
