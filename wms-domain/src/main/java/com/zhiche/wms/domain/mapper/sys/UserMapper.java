package com.zhiche.wms.domain.mapper.sys;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.domain.model.sys.UserDeliveryPoint;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface UserMapper extends BaseMapper<User> {

    List<Storehouse> listUserStorehouse(@Param("userId") Integer userId);

}
