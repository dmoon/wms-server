package com.zhiche.wms.domain.mapper.inbound;

import com.zhiche.wms.domain.model.inbound.InboundPutawayHeader;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 入库单头 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface InboundPutawayHeaderMapper extends BaseMapper<InboundPutawayHeader> {

}
