package com.zhiche.wms.domain.mapper.base;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.base.BusinessDocNumber;

import java.util.HashMap;

/**
 * <p>
 * 业务单据号 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-07
 */
public interface BusinessDocNumberMapper extends BaseMapper<BusinessDocNumber> {

    HashMap<String, Object> getNextByProcedure(HashMap<String, Object> params);
}
