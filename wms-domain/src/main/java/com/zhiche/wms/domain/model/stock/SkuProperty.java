package com.zhiche.wms.domain.model.stock;

import java.io.Serializable;

/**
 * Created by zhaoguixin on 2018/5/30.
 */
public class SkuProperty implements Serializable {

    private static final long serialVersionUID = 8959947951157240792L;
    /**
     * 货主
     */
    private String ownerId;
    /**
     * 物料ID
     */
    private String materielId;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 批号0
     */
    private String lotNo0;
    /**
     * 批号1
     */
    private String lotNo1;
    /**
     * 批号2
     */
    private String lotNo2;
    /**
     * 批号3
     */
    private String lotNo3;
    /**
     * 批号4
     */
    private String lotNo4;
    /**
     * 批号5
     */
    private String lotNo5;
    /**
     * 批号6
     */
    private String lotNo6;
    /**
     * 批号7
     */
    private String lotNo7;
    /**
     * 批号8
     */
    private String lotNo8;
    /**
     * 批号9
     */
    private String lotNo9;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }


    @Override
    public String toString() {
        return "Sku{" +
                ", ownerId=" + ownerId +
                ", materielId=" + materielId +
                ", uom=" + uom +
                ", lotNo0=" + lotNo0 +
                ", lotNo1=" + lotNo1 +
                ", lotNo2=" + lotNo2 +
                ", lotNo3=" + lotNo3 +
                ", lotNo4=" + lotNo4 +
                ", lotNo5=" + lotNo5 +
                ", lotNo6=" + lotNo6 +
                ", lotNo7=" + lotNo7 +
                ", lotNo8=" + lotNo8 +
                ", lotNo9=" + lotNo9 +
                "}";
    }
}
