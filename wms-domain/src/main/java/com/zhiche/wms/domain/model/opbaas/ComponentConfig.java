package com.zhiche.wms.domain.model.opbaas;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 随车工具配置
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-07
 */
@TableName("component_config")
public class ComponentConfig extends Model<ComponentConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 工具编码
     */
	private String code;
    /**
     * 工具名称
     */
	private String name;
    /**
     * 工具描述
     */
	private String describe;
    /**
     * 上级分类code
     */
	@TableField("parent_code")
	private String parentCode;
    /**
     * 级别(1:工具分类,2:工具名称)
     */
	private Integer level;
    /**
     * 排序
     */
	private String sort;
    /**
     * 状态(10：正常，20：禁用)
     */
	private String status;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 修改时间
     */
	@TableField("gmt_modified")
	private Date gmtModified;
	/**
	 * 是否标记(0：未标记，1：已标记)
	 */
	private String isReigister;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public String getIsReigister() {
		return isReigister;
	}

	public void setIsReigister(String isReigister) {
		this.isReigister = isReigister;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ComponentConfig{" +
			", id=" + id +
			", code=" + code +
			", name=" + name +
			", parentCode=" + parentCode +
			", level=" + level +
			", sort=" + sort +
			", status=" + status +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			"}";
	}
}
