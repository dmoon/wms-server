package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.dto.opbaas.resultdto.ExConfigurationDTO;
import com.zhiche.wms.domain.model.opbaas.ExceptionConfiguration;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 异常配置 Mapper 接口
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface ExceptionConfigurationMapper extends BaseMapper<ExceptionConfiguration> {

    List<ExConfigurationDTO> selectConfigurationListByParams(HashMap<String, Object> params);

}
