package com.zhiche.wms.app.controller.opbaas;


import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.dto.opbaas.paramdto.AppCommonQueryDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ReleaseWithShipmentDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import com.zhiche.wms.service.opbaas.IOrderReleaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 运单 前端控制器
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@RestController
@RequestMapping("/orderRelease")
public class OrderReleaseController {
    @Autowired
    private IOrderReleaseService releaseService;


    /**
     * 模糊查询发车点下的发运列表
     */
    @PostMapping("/queryReleaseShip")
    public RestfulResponse<Page<ReleaseWithShipmentDTO>> queryReleaseShipList(Page<ReleaseWithShipmentDTO> page) {
        Page<ReleaseWithShipmentDTO> dtoPage = releaseService.queryReleaseShipList(page);
        return new RestfulResponse<>(0, "success", dtoPage);
    }

    /**
     * 装车发运-详情
     */
    @PostMapping("/getReleaseShipDetail")
    public RestfulResponse<ReleaseWithShipmentDTO> getReleaseShipDetail(AppCommonQueryDTO dto) {
        ReleaseWithShipmentDTO detail = releaseService.getReleaseShipDetail(dto);
        return new RestfulResponse<>(0, "success", detail);
    }

    /**
     * 确认装车交验 - 不回传OTM
     */
    @PostMapping("/confirmReleaseShip")
    public RestfulResponse<Object> updateReleaseShip(AppCommonQueryDTO dto) {
        OtmOrderRelease data = releaseService.updateReleaseShip(dto);
        return new RestfulResponse<>(0, "success", data);
    }

    /**
     * 装车发运--模糊查询发车点下的发运列表
     */
    @PostMapping("/queryShipList")
    public RestfulResponse<Page<ShipmentForShipDTO>> queryShipList(Page<ShipmentForShipDTO> page) {
        Page<ShipmentForShipDTO> data = releaseService.queryShipList(page);
        return new RestfulResponse<>(0, "success", data);
    }

    /**
     * 装车发运--点击获取详情
     */
    @PostMapping("/getShipDetail")
    public RestfulResponse<ShipmentForShipDTO> getShipDetail(AppCommonQueryDTO dto) {
        ShipmentForShipDTO detail = releaseService.getShipDetail(dto);
        return new RestfulResponse<>(0, "success", detail);
    }


    /**
     * 装车发运--确认发运
     */
    @PostMapping("/updateShip")
    public RestfulResponse<ShipmentForShipDTO> updateShip(AppCommonQueryDTO dto) {
        ShipmentForShipDTO detail = releaseService.updateShip(dto);
        return new RestfulResponse<>(0, "success", detail);
    }

    /**
     * 快速赋码--绑定车架号  回传OTM
     */
    @PostMapping("/updateVin")
    public RestfulResponse<Object> updateVin(AppCommonQueryDTO dto){
        releaseService.updateVin(dto);
        return new RestfulResponse<>(0,"success",null);
    }


}

