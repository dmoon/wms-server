package com.zhiche.wms.app.test;

import com.zhiche.wms.AppApplication;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.inbound.InboundNoticeHeader;
import com.zhiche.wms.domain.model.inbound.InboundNoticeLine;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeHeader;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.service.base.IBusinessDocNumberService;
import com.zhiche.wms.service.constant.DocPrefix;
import com.zhiche.wms.service.constant.Status;
import com.zhiche.wms.service.inbound.IInboundNoticeHeaderService;
import com.zhiche.wms.service.inbound.IInboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundNoticeHeaderService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppApplication.class)
public class InboundNoticeTest {

    public Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private IInboundNoticeHeaderService inboundNoticeHeaderService;
    @Autowired
    private IInboundNoticeLineService inboundNoticeLineService;
    @Autowired
    private IOutboundNoticeHeaderService outboundNoticeHeaderService;
    @Autowired
    private IOutboundNoticeLineService outboundNoticeLineService;
    @Autowired
    private SnowFlakeId snowFlakeId;
    @Autowired
    private IBusinessDocNumberService businessDocNumberService;

    @Test
    public void createInboundNotice() throws InterruptedException {

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                logger.info("thread : {}  ----->start", Thread.currentThread().getName());
                InboundNoticeHeader inboundNoticeHeader = new InboundNoticeHeader();
                inboundNoticeHeader.setId(snowFlakeId.nextId());
                inboundNoticeHeader.setStoreHouseId(100030L);
                inboundNoticeHeader.setNoticeNo(businessDocNumberService.getInboundNoticeNo());
                businessDocNumberService.getInboundInspectNo();
                businessDocNumberService.getInboundPutAwayNo();
                businessDocNumberService.getMovementNo();
                businessDocNumberService.getOutboundNoticeNo();
                businessDocNumberService.getOutboundPrepareNo();
                businessDocNumberService.getOutboundShipNo();
                inboundNoticeHeader.setOrderDate(new Date());
                inboundNoticeHeader.setExpectSumQty(new BigDecimal("2"));
                inboundNoticeHeader.setLineCount(2);
                inboundNoticeHeader.setStatus(Status.Inbound.NOT);
                inboundNoticeHeader.setOwnerId(null);
                inboundNoticeHeader.setOwnerOrderNo("hanxiaohui");
                inboundNoticeHeader.setCarrierName("魏军水铁队自力运队");
                inboundNoticeHeader.setTransportMethod("平板四位板");
                inboundNoticeHeader.setPlateNumber("赣1234");
                inboundNoticeHeader.setDriverName("王二");
                inboundNoticeHeader.setDriverPhone("13333903333");
                inboundNoticeHeader.setExpectRecvDateLower(new Date());
                inboundNoticeHeader.setUom("台");


                InboundNoticeLine inboundNoticeLine = new InboundNoticeLine();
                inboundNoticeLine.setHeaderId(inboundNoticeHeader.getId());
                inboundNoticeLine.setOwnerId("1");
                inboundNoticeLine.setExpectQty(BigDecimal.ONE);
                inboundNoticeLine.setInboundQty(BigDecimal.ZERO);
                inboundNoticeLine.setMaterielId("10");
                inboundNoticeLine.setLotNo0("hanxiaohui1");
                inboundNoticeLine.setLotNo1("hanxiaohui1");
                inboundNoticeLine.setStatus(Status.Inbound.NOT);
                inboundNoticeLine.setId(snowFlakeId.nextId());
                inboundNoticeLine.setUom("台");

                InboundNoticeLine inboundNoticeLine1 = new InboundNoticeLine();
                inboundNoticeLine1.setHeaderId(inboundNoticeHeader.getId());
                inboundNoticeLine1.setOwnerId("1");
                inboundNoticeLine1.setExpectQty(BigDecimal.ONE);
                inboundNoticeLine1.setInboundQty(BigDecimal.ZERO);
                inboundNoticeLine1.setMaterielId("10");
                inboundNoticeLine1.setLotNo0("hanxiaohui2");
                inboundNoticeLine1.setLotNo1("hanxiaohui2");
                inboundNoticeLine1.setStatus(Status.Inbound.NOT);
                inboundNoticeLine1.setId(snowFlakeId.nextId());
                inboundNoticeLine1.setUom("台");
                inboundNoticeHeaderService.insert(inboundNoticeHeader);
                inboundNoticeLineService.insert(inboundNoticeLine);
                inboundNoticeLineService.insert(inboundNoticeLine1);
                logger.info("thread : {}  ----->end", Thread.currentThread().getName());
            }).start();

        }
        Thread.sleep(10000);
/*

        InboundNoticeLine inboundNoticeLine1 = new InboundNoticeLine();
        inboundNoticeLine1.setHeaderId(inboundNoticeHeader.getId());
        inboundNoticeLine1.setOwnerId("1");
        inboundNoticeLine1.setExpectQty(BigDecimal.ONE);
        inboundNoticeLine1.setMaterielId("20");
        inboundNoticeLine1.setLotNo0("1001");
        inboundNoticeLine1.setLotNo1("1002");
        inboundNoticeLine1.setStatus(Status.inbound.NOT);
        inboundNoticeLine1.setId(snowFlakeId.nextId());*/

    }

    @Test
    public void testDocNew() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                logger.info("thread : {}  ----->start", Thread.currentThread().getName());
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(), businessDocNumberService.updateNextByProcedure(DocPrefix.INBOUND_NOTICE));
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(),businessDocNumberService.updateNextByProcedure(DocPrefix.INBOUND_PUTAWAY));
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(),businessDocNumberService.updateNextByProcedure(DocPrefix.INBOUND_INSPECT));
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(),businessDocNumberService.updateNextByProcedure(DocPrefix.MOVEMENT));
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(),businessDocNumberService.updateNextByProcedure(DocPrefix.OUTBOUND_NOTICE));
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(),businessDocNumberService.updateNextByProcedure(DocPrefix.OUTBOUND_PREPARE));
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(),businessDocNumberService.updateNextByProcedure(DocPrefix.OUTBOUND_SHIP));
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(),businessDocNumberService.updateNextByProcedure(DocPrefix.STOCK_INIT));
                logger.info("thread : {}  ----->, value : {}", Thread.currentThread().getName(),businessDocNumberService.updateNextByProcedure(DocPrefix.STOCK_ADJUST));
            }).start();
        }
        Thread.sleep(1000L);
    }


    @Test
    public void createOutboundNotice() {
        OutboundNoticeHeader outboundNoticeHeader = new OutboundNoticeHeader();
        outboundNoticeHeader.setId(snowFlakeId.nextId());
        outboundNoticeHeader.setStoreHouseId(100030L);
        outboundNoticeHeader.setNoticeNo(businessDocNumberService.getInboundNoticeNo());
        outboundNoticeHeader.setOrderDate(new Date());
        outboundNoticeHeader.setExpectSumQty(new BigDecimal("2"));
        outboundNoticeHeader.setLineCount(2);
        outboundNoticeHeader.setStatus(Status.Inbound.NOT);
        outboundNoticeHeader.setOwnerId(null);
        outboundNoticeHeader.setOwnerOrderNo("hanxiaohui");
        outboundNoticeHeader.setCarrierName("魏军水铁队自力运队");
        outboundNoticeHeader.setTransportMethod("平板四位板");
        outboundNoticeHeader.setPlateNumber("赣1234");
        outboundNoticeHeader.setDriverName("王二");
        outboundNoticeHeader.setDriverPhone("13333903333");
        outboundNoticeHeader.setExpectShipDateLower(new Date());
        outboundNoticeHeader.setUom("台");


        OutboundNoticeLine outboundNoticeLine = new OutboundNoticeLine();
        outboundNoticeLine.setHeaderId(outboundNoticeHeader.getId());
        outboundNoticeLine.setOwnerId("1");
        outboundNoticeLine.setExpectQty(BigDecimal.ONE);
        outboundNoticeLine.setOutboundQty(BigDecimal.ZERO);
        outboundNoticeLine.setMaterielId("10");
        outboundNoticeLine.setLotNo0("hanxiaohui1");
        outboundNoticeLine.setLotNo1("hanxiaohui1");
        outboundNoticeLine.setStatus(Status.Inbound.NOT);
        outboundNoticeLine.setId(snowFlakeId.nextId());
        outboundNoticeLine.setUom("台");
        OutboundNoticeLine outboundNoticeLine1 = new OutboundNoticeLine();
        outboundNoticeLine1.setHeaderId(outboundNoticeHeader.getId());
        outboundNoticeLine1.setOwnerId("1");
        outboundNoticeLine1.setExpectQty(BigDecimal.ONE);
        outboundNoticeLine1.setOutboundQty(BigDecimal.ZERO);
        outboundNoticeLine1.setMaterielId("10");
        outboundNoticeLine1.setLotNo0("hanxiaohui2");
        outboundNoticeLine1.setLotNo1("hanxiaohui2");
        outboundNoticeLine1.setStatus(Status.Inbound.NOT);
        outboundNoticeLine1.setId(snowFlakeId.nextId());
        outboundNoticeLine1.setUom("台");
/*

        InboundNoticeLine inboundNoticeLine1 = new InboundNoticeLine();
        inboundNoticeLine1.setHeaderId(inboundNoticeHeader.getId());
        inboundNoticeLine1.setOwnerId("1");
        inboundNoticeLine1.setExpectQty(BigDecimal.ONE);
        inboundNoticeLine1.setMaterielId("20");
        inboundNoticeLine1.setLotNo0("1001");
        inboundNoticeLine1.setLotNo1("1002");
        inboundNoticeLine1.setStatus(Status.inbound.NOT);
        inboundNoticeLine1.setId(snowFlakeId.nextId());*/

        outboundNoticeHeaderService.insert(outboundNoticeHeader);
        outboundNoticeLineService.insert(outboundNoticeLine);
        outboundNoticeLineService.insert(outboundNoticeLine1);
    }
}
