package com.zhiche.wms.service.opbaas.impl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.opbaas.ComponentConfigMapper;
import com.zhiche.wms.domain.model.opbaas.ComponentConfig;
import com.zhiche.wms.dto.opbaas.paramdto.MissingRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.MissingRegidterDetailDTO;
import com.zhiche.wms.service.opbaas.IComponentConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 随车工具配置 服务实现类
 * </p>
 *
 * @author hxh
 * @since 2018-08-07
 */
@Service
public class ComponentConfigServiceImpl extends ServiceImpl<ComponentConfigMapper, ComponentConfig> implements IComponentConfigService {

    @Override
    public List<MissingRegidterDetailDTO> queryComponent() {

        Wrapper<MissingRegidterDetailDTO> ew = new EntityWrapper();
        ew.eq("level1Level","1");
        ew.eq("level1Status", TableStatusEnum.STATUS_10.getCode());
        ew.eq("level2Status", TableStatusEnum.STATUS_10.getCode());
        ew.orderBy("CONVERT(`level1Sort`,SIGNED)");
        ew.orderBy("CONVERT(`level2Sort`,SIGNED)");

        List<MissingRegidterDetailDTO> dtoList = this.baseMapper.queryComponent(ew);

        return dtoList;
    }
}
