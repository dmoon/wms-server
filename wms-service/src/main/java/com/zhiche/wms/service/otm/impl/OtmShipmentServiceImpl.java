package com.zhiche.wms.service.otm.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.model.otm.OtmShipment;
import com.zhiche.wms.domain.mapper.otm.OtmShipmentMapper;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.zhiche.wms.service.otm.IOtmShipmentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 调度指令 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
@Service
public class OtmShipmentServiceImpl extends ServiceImpl<OtmShipmentMapper, OtmShipment> implements IOtmShipmentService {

    @Autowired
    private SnowFlakeId snowFlakeId;

    @Autowired
    private IOtmOrderReleaseService otmOrderReleaseService;

    @Override
    @Transactional
    public boolean insertShipment(OtmShipment otmShipment) {
        otmShipment.setId(snowFlakeId.nextId());
        for(OtmOrderRelease otmOrderRelease : otmShipment.getOtmOrderReleaseList()){
            otmOrderRelease.setId(snowFlakeId.nextId());
        }
        return otmOrderReleaseService.insertBatch(otmShipment.getOtmOrderReleaseList()) && insert(otmShipment);
    }

    /**
     * 模糊查询指令列表  -- 关联发车配置点
     *
     */
    @Override
    public List<ShipmentForShipDTO> queryShipmentReleaseList(Page<ShipmentForShipDTO> page, HashMap<String, Object> param, EntityWrapper<ShipmentForShipDTO> ew) {
        return baseMapper.queryShipmentReleaseList(page,param, ew);
    }

    /**
     * 装车发运-点击获取指令详情
     */
    @Override
    public List<ShipmentForShipDTO > getShipDetail(EntityWrapper<ShipmentForShipDTO > ew) {
        return baseMapper.getShipDetail(ew);
    }

    @Override
    public int countShipmentReleaseList(HashMap<String, Object> params, EntityWrapper<ShipmentForShipDTO> ew) {
        return baseMapper.countShipmentReleaseList(params, ew);
    }

    @Override
    public List<ShipmentForShipDTO> queryShipmentReleases(EntityWrapper<ShipmentForShipDTO> dataEW) {
        return baseMapper.queryShipmentReleases(dataEW);
    }
}
