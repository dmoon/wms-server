package com.zhiche.wms.service.inbound;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.inbound.InboundPutawayHeader;
import com.zhiche.wms.domain.model.sys.User;

/**
 * <p>
 * 入库单头 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IInboundPutawayHeaderService extends IService<InboundPutawayHeader> {

    /**
     * 按通知单明细行ID全部入库
     *
     * @param noticeLineId 行ID
     * @param inboundType  入库类型
     * @param genMethod    数据来源
     */
    InboundPutawayHeader updateByNoticeLineId(Long noticeLineId, String inboundType, String genMethod);

    boolean savePutAway(InboundPutawayHeader inboundPutawayHeader);

    boolean insertMovementByPutaway(InboundPutawayHeader inboundPutawayHeader, User loginUser);

    InboundPutawayHeader updateNoticeLineWithLocationId(Long noticeLineId, String inboundType, String genMethod, Long storeLoactionId);

}
