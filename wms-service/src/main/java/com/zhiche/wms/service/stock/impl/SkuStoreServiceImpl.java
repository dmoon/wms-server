package com.zhiche.wms.service.stock.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.stock.SkuStoreMapper;
import com.zhiche.wms.domain.model.stock.SkuStore;
import com.zhiche.wms.service.stock.ISkuStoreService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 停放位置信息 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-10-19
 */
@Service
public class SkuStoreServiceImpl extends ServiceImpl<SkuStoreMapper, SkuStore> implements ISkuStoreService {

}
