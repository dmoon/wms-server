package com.zhiche.wms.service.opbaas.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.opbaas.DriverMapper;
import com.zhiche.wms.domain.model.opbaas.Driver;
import com.zhiche.wms.service.opbaas.IDriverService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 司机信息 服务实现类
 * </p>
 *
 * @author user
 * @since 2018-06-06
 */
@Service
public class DriverServiceImpl extends ServiceImpl<DriverMapper, Driver> implements IDriverService {

}
