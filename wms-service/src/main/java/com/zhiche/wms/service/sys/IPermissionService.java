package com.zhiche.wms.service.sys;

import com.zhiche.wms.domain.model.sys.Permission;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 权限 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
public interface IPermissionService extends IService<Permission> {

    List<Permission> listAppPermission(Integer userId);

    List<Permission> listFistPermission(Integer userId);

    List<Permission> listChildPermission(Integer userId, Integer parentPid);

}
