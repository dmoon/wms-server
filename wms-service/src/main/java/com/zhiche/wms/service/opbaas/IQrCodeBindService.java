package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.dto.opbaas.paramdto.QrCodeParamDTO;
import com.zhiche.wms.domain.model.opbaas.OpQrCodeBind;

/**
 * <p>
 * 二维码绑定 服务类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface IQrCodeBindService extends IService<OpQrCodeBind> {

    void updateBindQrCode(QrCodeParamDTO dto);
}