package com.zhiche.wms.service.outbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.outbound.OutboundPrepareHeader;
import com.zhiche.wms.dto.outbound.PreparationListHeadDTO;
import com.zhiche.wms.dto.outbound.PreparationListDetailDTO;
import com.zhiche.wms.dto.outbound.OutboundPreparationParamDTO;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 出库备料单头 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
public interface IOutboundPrepareHeaderService extends IService<OutboundPrepareHeader> {

    Integer updateStatus(Long id, Long houseId, Integer status, BigDecimal damagedSum);

    void createPreparation(OutboundPreparationParamDTO dto);

    List<PreparationListDetailDTO> getPreparationDetail(OutboundPreparationParamDTO dto);

    void updateChangePreparation(OutboundPreparationParamDTO dto);

    void updatePrepareFinishByLineId(Long noticeLineId, String modifiedName);

    /**
     * 查询备料任务列表
     */
    Page<PreparationListHeadDTO> queryOutPreparePage(Page<PreparationListHeadDTO> page);

    void singlePreparation(OutboundPreparationParamDTO dto);

    /**
     * 查询未备料任务列表
     */
    Page<PreparationListHeadDTO> notPreparationList(Page<PreparationListHeadDTO> page);
}
