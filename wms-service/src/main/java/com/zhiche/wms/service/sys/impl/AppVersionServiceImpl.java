package com.zhiche.wms.service.sys.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.core.supports.enums.AppVersionTypeEnum;
import com.zhiche.wms.domain.mapper.sys.AppVersionMapper;
import com.zhiche.wms.domain.model.sys.AppVersion;
import com.zhiche.wms.service.sys.IAppVersionService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-17
 */
@Service
public class AppVersionServiceImpl extends ServiceImpl<AppVersionMapper, AppVersion> implements IAppVersionService {

    @Override
    public AppVersion getLastVersionAndroid() {
        EntityWrapper<AppVersion> ew = new EntityWrapper<>();
        ew.eq("ext_1", AppVersionTypeEnum.ANDROID.getCode())
                .orderBy("gmt_create", false)
                .orderBy("id", false);
        Page<AppVersion> page = new Page<>(1, 1);
        List<AppVersion> appVersions = this.baseMapper.selectPage(page, ew);
        if (CollectionUtils.isNotEmpty(appVersions)) {
            return appVersions.get(0);
        }
        return null;
    }

    @Override
    public AppVersion getLastVersionIos() {
        EntityWrapper<AppVersion> ew = new EntityWrapper<>();
        ew.eq("ext_1", AppVersionTypeEnum.IOS.getCode())
                .orderBy("gmt_create", false)
                .orderBy("id", false);
        Page<AppVersion> page = new Page<>(1, 1);
        List<AppVersion> appVersions = this.baseMapper.selectPage(page, ew);
        if (CollectionUtils.isNotEmpty(appVersions)) {
            return appVersions.get(0);
        }
        return null;
    }
}
