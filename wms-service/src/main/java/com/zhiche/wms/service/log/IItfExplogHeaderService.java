package com.zhiche.wms.service.log;


import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.log.ItfExplogHeader;

/**
 * <p>
 * 接口导出日志头 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
public interface IItfExplogHeaderService extends IService<ItfExplogHeader> {

    /**
     * 保存导出日志头和明细
     */
    void insertWithLines(ItfExplogHeader exH);
}
