package com.zhiche.wms.service.sys.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.wms.domain.model.sys.UserRole;
import com.zhiche.wms.domain.mapper.sys.UserRoleMapper;
import com.zhiche.wms.service.sys.IUserRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户角色关联表 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

    @Override
    public List<UserRole> listUserRole(Integer userId) {
        Wrapper<UserRole> ew = new EntityWrapper<>();
        ew.eq("user_id",userId);
        return selectList(ew);
    }
}
