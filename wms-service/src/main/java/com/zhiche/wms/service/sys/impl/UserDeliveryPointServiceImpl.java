package com.zhiche.wms.service.sys.impl;

import com.zhiche.wms.domain.model.sys.UserDeliveryPoint;
import com.zhiche.wms.domain.mapper.sys.UserDeliveryPointMapper;
import com.zhiche.wms.service.sys.IUserDeliveryPointService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户发车点权限表 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
@Service
public class UserDeliveryPointServiceImpl extends ServiceImpl<UserDeliveryPointMapper, UserDeliveryPoint> implements IUserDeliveryPointService {

}
