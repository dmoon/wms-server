package com.zhiche.wms.service.inbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.inbound.InboundPutawayLine;
import com.zhiche.wms.dto.inbound.InboundPutawayDTO;
import com.zhiche.wms.dto.inbound.LocChangeDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 入库单明细 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface IInboundPutawayLineService extends IService<InboundPutawayLine> {

    InboundPutawayDTO getPutWaylineById(Long id);

    Page<InboundPutawayDTO> queryLinePage(Page<InboundPutawayDTO> page);

    /**
     * 入库记录导出
     */
    List<InboundPutawayDTO> queryExportData(Map<String, String> condition);

    /**
     * 领取钥匙时获取信息
     */
    InboundPutawayDTO getReceiveKeyInfo(Map<String, String> condition);

    /**
     * 领取钥匙
     */
    void updateReceiveKey(Map<String, String> condition);

    /**
     * 领取钥匙时打印二维码
     */
    InboundPutawayDTO getKeyPrintInfo(Map<String, String> condition);

    /**
     * 查询调整记录
     */
    Page<LocChangeDTO> queryChangLocList(Page<LocChangeDTO> page);

    /**
     * 导出调整记录
     */
    List<LocChangeDTO> queryExportLocList(Map<String, Object> condition);
}
