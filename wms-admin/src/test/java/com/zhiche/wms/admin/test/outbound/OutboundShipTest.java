package com.zhiche.wms.admin.test.outbound;

import com.zhiche.wms.WebApplication;
import com.zhiche.wms.service.constant.ShipType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.outbound.IOutboundShipHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipLineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zhaoguixin on 2018/6/20.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= WebApplication.class)
public class OutboundShipTest {
    @Autowired
    private IOutboundShipHeaderService outboundShipHeaderService;
    @Autowired
    private IOutboundShipLineService shipLineService;

    @Test
    public void outboundByNoticeLineId(){
        shipLineService.shipByNoticeLineId(new Long("46716385618817024"),
                ShipType.NOTICE_SHIP, SourceSystem.HAND_MADE);
    }



}
