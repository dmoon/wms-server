package com.zhiche.wms.admin.surpports;

import com.zhiche.wms.service.base.IBusinessDocNumberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * Created by zhaoguixin on 2018/6/8.
 */

@Controller
@RequestMapping(value = "/getDocNo",produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public class DocNumber {

    @Autowired
    private IBusinessDocNumberService businessDocNumberService;

    @GetMapping(value = "/inboundNoticeNo")
    public String getInboundNoticeNo(){
        String no = businessDocNumberService.getInboundNoticeNo();
        System.out.println(no);
        return no;
    }

    @GetMapping(value = "/inboundPutAawayNo")
    public String getInboundPutAawayNo(){
        String no = businessDocNumberService.getInboundPutAwayNo();
        System.out.println(no);
        return no;
    }
}
