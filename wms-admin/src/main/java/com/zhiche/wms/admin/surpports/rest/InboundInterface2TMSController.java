package com.zhiche.wms.admin.surpports.rest;

import com.zhiche.wms.dto.base.ResultDTO;
import com.zhiche.wms.dto.interfacedto.UpdateInboundFromTMSDTO;
import com.zhiche.wms.service.log.IItfImplogHeaderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Map;

/**
 * 更新入库通知数据--tms
 */
@RestController
@RequestMapping(value = "/wmsInterface")
@Api(value = "/wmsInterface", description = "WMS系统提供给TMS系统的接口")
public class InboundInterface2TMSController {

    @Autowired
    private IItfImplogHeaderService iItfImplogHeaderService;

    @ApiOperation(value = "/updateInboundOrder", notes = "此接口用于TMS系统更新入库信息")
    @PostMapping(value = "/updateInboundOrder")
    public ResultDTO<Object> updateInboundOrder(UpdateInboundFromTMSDTO interfaceDTO,
                                                HttpServletRequest request) {
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "更新信息成功");
        String hKey = request.getHeader("encode-key");
        interfaceDTO.sethKey(hKey);
        ArrayList<Map<String, String>> result = iItfImplogHeaderService.updateInboundOrder(interfaceDTO);
        resultDTO.setData(result);
        return resultDTO;
    }

}
