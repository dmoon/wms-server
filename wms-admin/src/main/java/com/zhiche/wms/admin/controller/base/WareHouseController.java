package com.zhiche.wms.admin.controller.base;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.domain.model.base.Storehouse;
import com.zhiche.wms.domain.model.sys.UserStorehouse;
import com.zhiche.wms.dto.inbound.StoreAreaAndLocationDTO;
import com.zhiche.wms.dto.sys.StorehouseDTO;
import com.zhiche.wms.dto.sys.StorehouseNodeDTO;
import com.zhiche.wms.service.base.IStorehouseService;
import com.zhiche.wms.service.sys.IUserStorehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/warehouse")
public class WareHouseController {

    @Autowired
    private IUserStorehouseService storehouseService;
    @Autowired
    private IStorehouseService houseService;

    /**
     * 获取用户仓库配置
     */
    @GetMapping("/getUserWareHouses")
    public RestfulResponse<List<UserStorehouse>> getUserHouses() {
        List<UserStorehouse> userHouses = storehouseService.getUserHouses();
        return new RestfulResponse<>(0, "查询成功", userHouses);
    }

    @PostMapping("/queryAllArea")
    public RestfulResponse<List<StoreAreaAndLocationDTO>> queryAllArea(@RequestBody Map<String, String> condition) {
        List<StoreAreaAndLocationDTO> dto = storehouseService.queryAllArea(condition);
        return new RestfulResponse<>(0, "查询成功", dto);
    }

    @PostMapping("/listStoreHouse")
    public RestfulResponse<Page<Storehouse>> listStoreHouse(@RequestBody Page<Storehouse> page) {
        Page<Storehouse> data = houseService.listStoreHousePage(page);
        return new RestfulResponse<>(0, "查询成功", data);
    }

    @PostMapping("/allUsableStoreHouse")
    public RestfulResponse<Page<Storehouse>> allUsableStoreHouse(@RequestBody Page<Storehouse> page) {
        RestfulResponse<Page<Storehouse>> result = new RestfulResponse<>(0, "success");
        try {
            Page<Storehouse> storehouses = houseService.allUsableStoreHouse(page);
            result.setData(storehouses);
        } catch (Exception e) {
            result.setCode(-1);
            result.setMessage(e.getMessage());
            return result;
        }
        return result;
    }

    @PostMapping("/listNodeOption")
    public RestfulResponse<List<StorehouseNodeDTO>> getHoseInfo(@RequestBody Map<String,String> condition) {
        List<StorehouseNodeDTO> info = houseService.getHoseInfo(condition);
        return new RestfulResponse<>(0, "查询成功", info);
    }


    @PostMapping("/saveStoreHouse")
    public RestfulResponse<Object> saveStoreHouse(@RequestBody StorehouseDTO dto) {
        houseService.saveStoreHouse(dto);
        return new RestfulResponse<>(0, "操作成功", null);
    }

    @PostMapping("/updateStoreHouse")
    public RestfulResponse<Object> updateStoreHouse(@RequestBody StorehouseDTO dto) {
        houseService.updateStoreHouse(dto);
        return new RestfulResponse<>(0,"操作成功",null);
    }
}
