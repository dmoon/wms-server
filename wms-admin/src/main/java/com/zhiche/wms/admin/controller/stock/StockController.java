package com.zhiche.wms.admin.controller.stock;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.stock.StockDTO;
import com.zhiche.wms.service.stock.IStockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by zhaoguixin on 2018/7/7.
 */
@Controller
@RequestMapping(value = "/stock", produces = MediaType.APPLICATION_JSON_VALUE)
public class StockController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private IStockService stockService;

    @PostMapping(value = "/queryPage", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<Page<StockDTO>> queryPage(@RequestBody Page<StockDTO> page) {
        LOGGER.info("Controller:stock/queryPage data: {}", page.toString());
        RestfulResponse<Page<StockDTO>> result = new RestfulResponse<>(0, "success");
        try {
            Page<StockDTO> stockDTOPage = stockService.queryPageStock(page);
            result.setData(stockDTOPage);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    @PostMapping(value = "/lockStock", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<List<String>> lockStock(@RequestBody List<Long> stockIds) {
        LOGGER.info("Controller:stock/lockStock data: {}`", stockIds.toString());
        RestfulResponse<List<String>> result = new RestfulResponse<>(0, "success");
        try {
            stockService.lockStockBatch(stockIds);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    @PostMapping(value = "/unlockStock", consumes = "application/json")
    @ResponseBody
    public RestfulResponse<List<String>> unlockStock(@RequestBody List<Long> stockIds) {
        LOGGER.info("Controller:stock/unlockStock data: {}", stockIds.toString());
        RestfulResponse<List<String>> result = new RestfulResponse<>(0, "success");
        try {
            stockService.unlockStockBatch(stockIds);
        } catch (Exception ex) {
            result.setCode(-1);
            result.setMessage("error");
        }
        return result;
    }

    /**
     * 更新库位
     */
    @PostMapping("/updateStockLocation")
    @ResponseBody
    public RestfulResponse<Object> updateStockLocation(@RequestBody Map<String, String> condition) {
        stockService.updateStockLocation(condition);
        return new RestfulResponse<>(0, "调整成功");
    }

    /**
     * 库存导出
     */
    @PostMapping(value = "/exportStockData")
    @ResponseBody
    public RestfulResponse<List<StockDTO>> exportStockData(@RequestBody Map<String, String> condition) {
        RestfulResponse<List<StockDTO>> result = new RestfulResponse<>(0, "success");
        List<StockDTO> data = stockService.exportStockData(condition);
        result.setData(data);
        return result;
    }


}
